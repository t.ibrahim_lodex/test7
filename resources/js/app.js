require('./bootstrap');

window.Vue = require('vue');
import router from './router'

Vue.component('first_page', require('./components/firstPage.vue').default)
 
const app = new Vue({
    el : "#app", 
    router
})
