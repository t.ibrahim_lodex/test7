import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import users from './views/users'  
import veichles from './views/veichles'  

const routes = [
    
    {
        path : '/admin-users',
        component: users
    },
    {
        path : '/admin-veichles',
        component: veichles
    }
    
]

export default new Router({
    mode: 'history',
    routes
})